import React from 'react'
import {Container} from 'react-bootstrap'; 

export default function AboutMe() {
	return(
    <Container className="aboutMe my-5 col-lg-12">
		<div className="row p-5 text-dark">
			<div className = "col-lg-6 mx-auto d-flex-column text-center">
				<img src="./images/Me.jpg" className="rounded-circle me" alt="About Me"/>
				<h4 className="display-4 my-3">ABOUT ME</h4>
				<p className="text-justify lead text-wrap ">As I was growing up, I've always been into computer programs, web applications, designing & creating regardless of its outcome. This leads me to take a course in Web Development at Zuitt Philippines where creativity in design and back-end processes are essentials.</p>
				
				<p className="lead text-justify">Now, after graduating, I know I am now fully equipped with all the things I've learned and the passion to do what I love. I know that there's still a huge room for improvement and new knowledge. My previous work experiences in customer relation, financial aspect, SEO, web designs and more combined with all the things I've learned from this Boot camp has led me to be fully ready to take this new journey. Learning does not end once you achieve your goal. I believe that this mind set has helped me improve my work and my way of life. </p>
			</div>

			<div className="col-lg-6 d-flex-column text-center pt-5 py-5">
				<h4 className="display-4 my-3">SKILL SET</h4>
				<div className="justify-content-between py-4">
					<img src="./images/skill1.svg" className="img-thumbnail skill mx-3 responsive-image" alt="skills icon"/>
					<img src="./images/skill2.svg" className="img-thumbnail skill mx-3 responsive-image" alt="skills icon"/>
					<img src="./images/skill3.svg" className="img-thumbnail skill mx-3 responsive-image" alt="skills icon"/>
				</div>
				<div className="justify-content-between py-4">
					<img src="./images/skill4.svg" className="img-thumbnail skill mx-3 responsive-image" alt="skills icon"/>
					<img src="./images/skill5.svg" className="img-thumbnail skill mx-3 responsive-image" alt="skills icon"/>
					<img src="./images/skill6.svg" className="img-thumbnail skill mx-3 responsive-image" alt="skills icon"/>
					<img src="./images/skill7.svg" className="img-thumbnail skill mx-3 responsive-image nosm" alt="skills icon"/>
				</div>
				<div className="justify-content-between py-4 lands">
					<img src="./images/skill8.svg" className="img-thumbnail skill mx-3 responsive-image lands" alt="skills icon"/>
					<img src="./images/skill9.svg" className="img-thumbnail skill mx-3 responsive-image lands" alt="skills icon"/>
					<img src="./images/skill10.svg" className="img-thumbnail skill mx-3 responsive-image lands none" alt="skills icon"/>
				</div>
			</div>
		</div>
	</Container>
	)
}
