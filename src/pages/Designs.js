import React from 'react'
import {Container} from 'react-bootstrap'; 
import { Row } from 'react-bootstrap'
import { Col } from 'react-bootstrap'
import Carousel from 'react-bootstrap/Carousel'

export default function Banner() {
	return(
	<Container className="aboutMe col-lg-12">
		<Row>
			{/*<!-- Works Title -->*/}
			<Col className="col-lg-4 col-md-10 col-sm-12 mx-auto">
				<h2 className="exp d-flex justify-content-center display-4 text-center mt-5 text-white">EXPERIENCE</h2>
				<img className="mx-auto d-block works center" src="./images/myworks.svg"/>
				<h2 className="exp d-flex justify-content-center display-4 text-center mt-2 text-white">MY DESIGNS</h2>
				  <ul className="pagination pagination-lg mt-5 d-block justify-content-center text-right">
				    <li className="caro-i page-item i text-center py-2">
				    	<a className="caro-i a1 page-link" href="https://kristel_muyco.gitlab.io/capstone-one/" target="_blank" data-hover="Portfolio Development (HTML, CSS & Bootstrap)" data-active="I'M ACTIVE">
				    	<span>Capstone One</span>
				    	</a>
				    </li>
				    <li className="caro-i page-item i text-center py-2">
				    	<a className="caro-i a1  page-link" href="https://kristel_muyco.gitlab.io/capstone-two/" target="_blank" data-hover="(Course Booking App) MEN Stack" data-active="I'M ACTIVE">
				    	<span>Capstone Two</span>
				    	</a>
				    </li>
				    <li className="caro-i page-item i text-center py-2">
				    	<a className="caro-i a1 page-link" href="https://kristel_muyco.gitlab.io/capstone-one/" target="_blank" data-hover="(Developer's Portfolio)React JS" data-active="I'M ACTIVE">
				    	<span>React Mini Capstone</span>
				    	</a>
				    </li>
				  </ul>
			</Col>
			{/*<!-- Carousel -->*/}
			<Col className="col-md-8 mt-5 none">
					{/*<!-- Carousel -->*/}
				<Carousel className="carousel carocon" id="carouselSlides" data-ride="carousel">
							<Carousel.Item interval={1000} active>
								<img src="./images/caro1.png" alt="carousel img" id="carouselpics" className="d-block w-100 h-100 rounded"/>
							</Carousel.Item>
  							<Carousel.Item interval={1000}>
								<img src="./images/caro2.png" alt="carousel img" id="carouselpics" className="d-block w-100 rounded"/>
							</Carousel.Item>
							<Carousel.Item interval={1000}>
								<img src="./images/caro3.png" alt="carousel img" id="carouselpics" className="d-block w-100 h-100 rounded"/>
							</Carousel.Item>
				</Carousel>
			</Col>
		</Row>
	</Container>
	)
}
