import React from 'react';
import {Container} from 'react-bootstrap'; 
import { Row } from 'react-bootstrap';
import { Col } from 'react-bootstrap';
import Button from 'react-bootstrap/Button';

export default function footerPage() {
	return(
<>
	<footer className="bg-dark text-white d-flex justify-content-center pt-3 pl-3 pb-5 mx-auto">
		<Row className="text-left mx-auto">		
			<Col className="col-xs-12 col-sm-3 col-md-3 col-lg-3">
				<h4>About Us</h4>
				<hr className="bg-light accent-2 mb-4 mt-0 d-inline-block mx-auto cont-line"/>
				<div className="w-md-25 w-lg-25"><i>“A designer knows he has achieved perfection not when there is nothing left to add, but when there is nothing left to take away.”</i>
				<br/>
					<img className="responsive-image pb-2 mt-2 d-block footpic" src="./images/kbmlogo-light.svg" alt="footpic"/>
				</div>
			</Col>
			<Col className="col-xs-12 col-sm-2 col-md-3 col-lg-3">
				<h4>Content</h4>
				<hr className="bg-light accent-2 mb-4 mt-0 d-inline-block mx-auto cont-line"/>
				<p><a className="text-white text-decoration-none" href="https://kristel_muyco.gitlab.io/capstone-one/" target="_blank">Portfolio</a></p>
				<p><a className="text-white text-decoration-none" href="https://kristel_muyco.gitlab.io/capstone-one/" target="_blank">About Kristel</a></p>
				<p><a className="text-white text-decoration-none" href="https://kristel_muyco.gitlab.io/capstone-one/" target="_blank">Skills</a></p>
				<p><a className="text-white text-decoration-none" href="https://kristel_muyco.gitlab.io/capstone-one/" target="_blank">Projects</a></p>
			</Col>
			<Col className="col-xs-12 col-sm-3 col-md-3 col-lg-3">
				<h4>Follow Us</h4>
				<hr className="bg-light accent-2 mb-4 mt-0 d-inline-block mx-auto cont-line"/>
				<p><a className="text-white text-decoration-none"href="https://www.messenger.com/kbmuyco" target="_blank"><i className="fab fa-gitlab text-warning pr-2"></i>Messenger</a></p>
				<p><a className="text-white text-decoration-none"href="https://www.instagram.com/kmtreasured/" target="_blank"><i className="fab fa-instagram text-warning pr-2"></i> Instagram</a></p>
				<p><a className="text-white text-decoration-none"href="https://www.linkedin.com/in/kristel-muyco-549a07193/" target="_blank"><i className="fab fa-linkedin text-warning pr-2"></i> LinkedIn</a></p>
				<p><a className="text-white text-decoration-none"href="https://gitlab.com/kristel_muyco" target="_blank"><i className="fab fa-gitlab text-warning pr-2"></i> Gitlab</a></p>
			</Col>
			<Col className="col-xs-12 col-sm-4 col-md-3 col-lg-3 pr-3">
				<h4>Contacts Us</h4>
				<hr className="bg-light accent-2 mb-4 mt-0 d-inline-block mx-auto cont-line"/>
				<p><a className="text-white text-decoration-none" href="#" target="_blank">(02) 8 282 9520</a></p>
				<p><a className="text-white text-decoration-none"href="#" target="_blank">0927 952 9749 (Globe)</a></p>
				<p><a className="text-white text-decoration-none" href="#" target="_blank">0998 938 8160 (Smart)</a></p>
				<p><a className="none text-white text-decoration-none" href="#" target="_blank">kbmuyco.0213@gmail.com</a></p>
				<Button className="btn-warning btn-md">Get in Touch! </Button>
			</Col>
			<hr className="bg-light accent-3 my-2 d-inline-block align-center mx-auto"/>
		</Row>

	</footer>

	<footer className=" footer bg-dark text-white text-center mx-auto d-flex justify-content-center">
		<p>&#169; 2020 - 2022 KBMDesign. All Rights Reserved | Terms of Use | Privacy Policy | Celebrating 2 Weeks of Digital Marketing Excellence </p>
	</footer>

</>
		)
}