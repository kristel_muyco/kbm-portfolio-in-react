import React from 'react'
import {Container} from 'react-bootstrap'; 
import { Row } from 'react-bootstrap'
import { Col } from 'react-bootstrap'
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';

export default function ContactPage() {
	return(
	<Container className="landing col-lg-12 py-5">
		<Row>
			{/*<!-- Contact Form -->*/}
			<Col className="ml-5 col-lg-7 col-md-10 col-sm-10">
				<div className="card my-auto">
					<h2 className="card-header text-center text-monospace">Tell Us Something About You</h2>
					<div className="card-body d-block p-5">
						<p className="text-monospace mb-5">Hello Kristel,</p>
						<Form className="form-inline text-justify p-0 word-wrap">
							<Form.Label className="text-monospace mr-1 pb-4">My name is </Form.Label>
							<Form.Control className="col-auto bg-transparent mb-4 text-white text-monospace" type="text"/>

							<Form.Label className="text-monospace mr-1 pb-4">. I am a </Form.Label>
							<Form.Control id="role" as="select" className="col-auto bg-transparent text-monospace mb-4 mr-1">
								<option className="text-monospace text-center">---</option>
								<option className="text-monospace">A Businessman</option>
								<option className="text-monospace">A Student</option>
								<option className="text-monospace">A Developer</option>
								<option className="text-monospace">A Designer</option>
							</Form.Control> 

							<Form.Label className="text-monospace mr-1 pb-4"> who shows interest in your works.  </Form.Label>
							<Form.Label className="text-monospace mr-1 pb-4">It caught my eyes and saw your story, skills and projects.  </Form.Label>
							<Form.Label className="text-monospace mr-2 pb-4">It interest me to see your site because of </Form.Label>
							<Form.Control id="interest" as="select" className="bg-dark mr-1 col-auto bg-transparent text-monospace mb-4">
								<option className="text-monospace text-center">---</option>
								<option className="text-monospace">Your Portfolio</option>
								<option className="text-monospace">Your Story</option>
								<option className="text-monospace">Your Skillset</option>
								<option className="text-monospace">Your Designs</option>
							</Form.Control>

							<Form.Label className="text-monospace mr-1 pb-4"> Please contact me by </Form.Label>
							<Form.Control id="interest" as="select" className="bg-dark mr-1 col-auto bg-transparent text-monospace mb-4">
								<option className="text-monospace text-center">---</option>
								<option className="text-monospace">Email</option>
								<option className="text-monospace">Mobile</option>
								<option className="text-monospace">Mobile & Email</option>
							</Form.Control>

							<Form.Label className="text-monospace mr-1 pb-4"> Here are my contact details and information: </Form.Label>
							<Form.Control className="col-auto bg-transparent mb-4 text-white text-monospace" type="text"/>
							<br/>
							<br/>
							<Button variant="warning" type="submit" className="btn btn-lg ml-auto mt-5"><i className="fa fa-arrow-right px-3 h-100"></i></Button>
						</Form>
					</div>
				</div>
			</Col>

			{/*<!-- Hello Pic -->*/}
			<Col className="col-md-4 m-auto p-auto nosm">
				<img src="https://media.giphy.com/media/Cmr1OMJ2FN0B2/giphy.gif" className="rounded d-flex mx-auto  hello d-xs-none d-sm-block my-auto" alt="hello icon"/>
			</Col>
		</Row>
	</Container>
		)
}