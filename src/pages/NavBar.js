import React from 'react'
import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'

export default function NavBar(){
	return(
			<Navbar className="bg-dark" expand="lg">
				<Navbar.Brand href="#">
					<img src="images/kbmlogo-light.svg" className="retina" alt="logo" alt="svg logo"/>
				</Navbar.Brand>
				<Navbar.Toggle className="text-white" aria-controls="basic-navbar-nav" />
				<Navbar.Collapse id="basic-navbar-nav">
					<Nav className="ml-auto">
						<Nav.Link className="a ml-auto" active href="/">Home</Nav.Link>
						<Nav.Link className="a ml-auto" href="about">About Kristel</Nav.Link>
						<Nav.Link className="a ml-auto" href="offer">What I Offer</Nav.Link>
						<Nav.Link className="a ml-auto" href="designs">Designs</Nav.Link>
						<Nav.Link className="a ml-auto" href="contact">Get in Touch</Nav.Link>
					</Nav>
				</Navbar.Collapse>
			</Navbar>
		)
}
