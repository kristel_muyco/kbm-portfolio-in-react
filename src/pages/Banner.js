import React from 'react'
import {Container} from 'react-bootstrap'; 
import Jumbotron from 'react-bootstrap/Jumbotron'
import Button from 'react-bootstrap/Button'
import Nav from 'react-bootstrap/Nav'

import AboutMe from './AboutMe';
import SkillCards from './SkillCards';
import Designs from './Designs';
import ContactPage from './ContactPage';

export default function Banner() {
	return(
	<>
	<Container className="landing">
		<Jumbotron className="Jumbotron mx-auto my-5 text-center">
		     <h3 className="display-4 ml3">GET TO KNOW ME</h3>
		     <hr className="bg-light border border-white mx-auto"/>
		     <p> <small className="lead text-justify text-center name">KRISTEL MUYCO</small><br/></p>		     
		     <Button className="btn btn-lg mt-4 btn-warning"><Nav.Link className="a ml-auto" href="about">Let's Go! </Nav.Link></Button>
		</Jumbotron>
	> 
    </Container>
            <AboutMe /> 
            <SkillCards  /> 
            <Designs /> 
            <ContactPage /> 
    </>
	)
}
