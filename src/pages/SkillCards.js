import React from 'react'
import { Row } from 'react-bootstrap'
import { Col } from 'react-bootstrap'
import { Card } from 'react-bootstrap'
import {Container} from 'react-bootstrap'; 

export default function SkillCards() {
	return(
	<Container className="landing col-lg-12 pb-5">
		<Row>
			<Col>
				<h1 className="text-center text-white mb-5 py-5 display-4"><small>WHAT I CAN DO</small><br/> FOR YOU</h1>
				<div className="card-deck text-center mx-auto mt-5">
					<Card className="card mx-3">
						<Card.Header className="text-warning">UX RESEARCH</Card.Header>
							<img className="card-img-top icons mx-auto" src="./images/1.svg" alt="thumbnail"/>
						<Card.Body className="text-center text-justify">
							Gathering qualitative and quantitative data to understand the end-users, their needs, desires and frustrations.
						</Card.Body>
					</Card>
					<Card className="card mx-3">
						<Card.Header className="text-warning">INTERACTION DESIGN</Card.Header>
							<img className="card-img-top icons mx-auto" src="./images/3.svg" alt="thumbnail"/>
						<Card.Body className="text-center text-justify">
							Editing and testing many possible solutions that aim to solve business and user needs. Validating solutions in the form of user-testing.
						</Card.Body>
					</Card>
					<Card className="card mx-3">
						<Card.Header className="text-warning">VISUAL DESIGN</Card.Header>
							<img className="card-img-top icons mx-auto" src="./images/2.svg" alt="thumbnail"/>
						<Card.Body className="text-center text-justify">
							Creating and maintaining a visual design language for the product which communicates core brand values.
						</Card.Body>
					</Card>
				</div>
			</Col>
		</Row>
	</Container>
		)
}
