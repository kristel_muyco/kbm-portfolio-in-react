import React from 'react';
import NavBar from './pages/NavBar';
import Banner from './pages/Banner';
import AboutMe from './pages/AboutMe';
import SkillCards from './pages/SkillCards';
import Designs from './pages/Designs';
import ContactPage from './pages/ContactPage';
import FooterPage from './pages/Footer';

import {BrowserRouter as Router} from 'react-router-dom';
import {Route, Switch} from 'react-router-dom';

import './App.css';

function App() {
  return (
    <div className="App">
        <Router>
            <NavBar />
          <Switch>
            <Banner exact path="/" components={Banner} /> 
            <AboutMe exact path="/about" component={AboutMe} /> 
            <SkillCards exact path="/offer" component={SkillCards} /> 
            <Designs exact path="/designs" component={Designs} /> 
            <ContactPage exact path="/contact" component={ContactPage} />  
          </Switch>
            <FooterPage />
        </Router>    
    </div>
  );  
}

export default App;
